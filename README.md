# Pisi Linux Notları

Pisi Linux için geliştirme yaparken yardımcı notlar

## Paketlemeye yardımcı notlar:
PartOf paketin nereye ait olduğunu gösteriyor. Kullanılmazsa da olur ve bir `uyarı - warning` verir pisi!
```xml
<PartOf>system.base</PartOf>
<PartOf>network.analyzer</PartOf>
<PartOf>multimedia.graphics</PartOf>
<PartOf>security.web.spoofer</PartOf>
<PartOf>security.cracking.cracker</PartOf>
<PartOf>programming.language.perl</PartOf>
<PartOf>programming.language.python</PartOf>
<PartOf>programming.language.python3</PartOf>
```

History altına eklenecek etiketler: Release, sürüm ve tarih deneteimi yapmayı unutma!
```xml
        <Update release="5">
            <Date>2020-09-08</Date>
            <Version>4.0.0</Version>
            <Comment>Rebuild due to new ruby version</Comment>
            <Name>Blue Devil</Name>
            <Email>bluedevil@sctzine.com</Email>
        </Update>

        <Update release="1">
            <Date>2020-09-08</Date>
            <Version>0.028</Version>
            <Comment>First Pisi Release</Comment>
            <Name>Blue Devil</Name>
            <Email>bluedevil@sctzine.com</Email>
        </Update>

        <Update release="5">
            <Date>2020-09-22</Date>
            <Version>0.51.0</Version>
            <Comment>Patch version bump.</Comment>
            <Name>Blue Devil</Name>
            <Email>bluedevil@sctzine.com</Email>
        </Update>
```

### Yama eklemek:
```xml
        <Patches>
            <Patch>config.patch</Patch>
            <Patch level="1">pygame-1.9.1-config.patch</Patch>unbundle-sip.patch
        </Patches>
```

### Ek dosyalar eklemek
Bazen elle bazı dosyaları pisi paketinin içine atmak gerekebilir. Etiket içindeki dosya adı `files` dizini altındaki dosyadır. Nereye hangi izinler ile atılmak isteniyorsa etiket içindeki parametreler ona göre ayarlanmalıdır.
```xml
        <AdditionalFiles>
            <AdditionalFile owner="root" target="/usr/share/pspp/pspp.desktop" group="root" permission="0644">pspp.desktop</AdditionalFile>
            <AdditionalFile owner="root" target="/usr/share/pspp/splash.png" permission="0644">splash.png</AdditionalFile>
        </AdditionalFiles>
        
        <AdditionalFiles>
            <AdditionalFile owner="root" permission="0644" target="/usr/share/applications/mediainfo.desktop">mediainfo.desktop</AdditionalFile>
        </AdditionalFiles>
```

### Çeviri eklemek
```xml
<?xml version="1.0" ?>
<PISI>
    <Source>
        <Name></Name>
        <Summary xml:lang="tr"></Summary>
        <Description xml:lang="tr"></Description>
    </Source>

    <Package>
        <Name>libnotify-docs</Name>
        <Summary xml:lang="tr">libnotify için belgelendirme dosyaları</Summary>
        <Description xml:lang="tr">libnotify-docs, libnotify için belgelendirme dosyalarını içerir.</Description>
    </Package>

    <Package>
        <Name>libnotify-devel</Name>
        <Summary xml:lang="tr">libnotify için geliştirme dosyaları</Summary>
        <Description xml:lang="tr">libnotify-devel, libnotify için geliştirme dosyalarını içerir.</Description>
    </Package>

    <Package>
        <Name>libsdl-32bit</Name>
        <Summary xml:lang="tr">libsdl için 32-bit paylaşımlı kitaplıklar</Summary>
        <Description xml:lang="tr">libsdl-32bit, libsdl için 32-bit paylaşımlı kitaplıklar sunar.</Description>
    </Package>
</PISI>
```

### Kaynaktan "DEVEL" paketi çıkartmak
Bazı uygulama veya kitaplıklar aynı zamanda geliştirme başlıklarını da sunar. Her kullanıcı geliştirici olmadığına göre bu başlık(*.h uzantılı `header` dosyaları) dosyalarının sisteme yüklenmesine gerek yoktur. Bu dosyaları ana paketten `-devel` yazarak ayırıyoruz.
```xml
    <Package>
        <Name>source-highlight-devel</Name>
        <Summary>Development files for source-highlight</Summary>
        <RuntimeDependencies>
            <Dependency release="current">source-highlight</Dependency>
        </RuntimeDependencies>
        <Files>
            <Path fileType="header">/usr/include</Path>
            <Path fileType="data">/usr/lib/pkgconfig</Path>
        </Files>
    </Package>
```

### Kaynaktan "DOCS" paketi çıkartmak
Bazı kaynaklar kullanıcıları için belgelendirmeler hazırlayıp kaynak paketine koyabilir. Böyle durumlarda `-docs` son eki ile paketi ayırıyoruz.
```xml
    <Package>
        <Name>xapian-core-docs</Name>
        <IsA>data:doc</IsA>
        <Summary>Documentation files for xapian-core</Summary>
        <Description>xapian-core-docs provides documentation files for xapian-core.</Description>
        <RuntimeDependencies>
            <Dependency release="current">xapian-core</Dependency>
        </RuntimeDependencies>
        <Files>
            <Path fileType="doc">/usr/share/doc/xapian-core/apidoc.pdf</Path>
            <Path fileType="doc">/usr/share/doc/xapian-core/apidoc/</Path>
            <Path fileType="doc">/usr/share/doc/xapian-core/*html</Path>
        </Files>
    </Package>
```

### Bash betikleri çalıştırmak
Bazen  geliştirici `autogen.sh` veya `bootstrap.sh` adlarında betik hazırlayıp derlenme işleminin bunlar aracılığıyla başlatılmasını isteyebilir. `configure`den önce bunlar çalıştırılmalıdır. Sonrasında ise `configure`. Yine de yapılacaklar paketten pakete değişecektir.
```python
    shelltools.system("./autogen.sh")
    shelltools.system("./bootstrap.sh")
```

### To fix unused direct dependency analysis
Pisi paketi oluştuktan sonra `checkelf` ile paketin bağımlılıklarının doğru bir şekilde eklenip eklenmediğinin denetimi yapılır. Bu esnada `runpath` ve `direct dependency` analizleri de yapılır. Bunları düzeltmek her paket için ayrı ayrı ve pakete özeldir.<br />
`configure`dan sonra `libtool` dosyası oluşuyorsa aşağıdaki komut kullanılabilir.
```python
    # fix unused direct dependency analysis
    pisitools.dosed("libtool", " -shared ", " -Wl,-O1,--as-needed -shared ")
```
Bazen `configure` işleminden önce `derleyici - compiler` için `LDSHARED` çevre değişkeninin aşağıdaki gibi kurulması gerekir. Kurulan değer yine paketten pakete değişiklik gösterebilir.
```python
    # fix unused direct dependency analysis
    shelltools.export("LDSHARED", "x86_64-pc-linux-gnu-gcc -Wl,-O1,--as-needed -shared -lpthread -Wl,-O1 -Wl,-z,relro -Wl,--hash-style=gnu -Wl,--as-needed -Wl,--sort-common")
```

### To suppress compiler warnings
Bu çok gereksiz ve önemsiz bir ayrıntı. Paket derlenirken o kadar çok uyarı veriyorki neyin ne olduğunu anlayamıyorsunuz. Hele bir de paket `error - hata` veriyorsa. Zaten debug mod da açık, araki hatanın nerede olduğunu bulasın. Böyle durumlarda derleyiciye `hata uyarılarını` gösterme diyebiliriz:
```python
    # suppress compiler warnings
    pisitools.cflags.add("-Wno-unused-result \
                          -Wno-format-overflow \
                          -Wno-stringop-overflow \
                          -Wno-stringop-truncation \
                          -Wno-maybe-uninitialized \
                          -Wno-alloc-size-larger-than \
                          -Wno-aggressive-loop-optimizations")
```
Bazen pisitools.cflags.add komutu bayrakları kuramıyor o zaman da shelltoolsdan yardım alınabilir:
```python
    # suppress compiler warnings
    shelltools.export("CFLAGS", "-fno-strict-aliasing -mtune=generic -march=x86-64 -O2 -pipe -fstack-protector -D_FORTIFY_SOURCE=2 -g -fPIC -fwrapv -DNDEBUG -g -fwrapv -O3 -Wno-strict-aliasing")
```

### Seçenekler eklemek

```python
i = "-DBUILD_SHARED_LIBS=ON \
     -DCMAKE_BUILD_TYPE=Release \
     -DCMAKE_INSTALL_LIBDIR=lib \
     -DCMAKE_INSTALL_PREFIX=/usr \
    "
```
ya da (çift tırnağa dikkat! cmaketools ıle derlerken gerekiyor ama mesela cflags.add ile eklerken gerekmiyor.)
```python
    options = ''.join([
              '"-Wno-array-bounds ',
              '-Wno-uninitialized ',
              '-Wno-unused-variable ',
              '-Wno-pessimizing-move ',
              '-Wno-init-list-lifetime ',
              '-Wno-cast-function-type ',
              '-Wno-stringop-truncation ',
              '-Wno-nonportable-include-path"'
              ])
```
*ÖNEMLİ*<br />
Özellikle cmake kullanırken değişkenler ile atamak istediğimiz değerler arasına boşluk bırakmamak gerekiyor. Aksi durumda değer ataması yapılmıyor. Örneğin `-DCMAKE_CXX_FLAGS=%s \` atamasında eğer = ile %s arasına boşluk koyulursa atama yapılmıyor. Ayrıca birden çok değişken ataması yapılacaksa üstten tırnak ile ayırmak gerekiyor. Ve yine = 'den hemen sonra tırnak işaretinin başlaması gerekiyor. Doğru yapıldığında sonuç:
```bash
DEBUG: return value for "cmake -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_C_FLAGS="-mtune=generic -march=x86-64 -O2 -pipe -fstack-protector -D_FORTIFY_SOURCE=2 -g -fPIC" -DCMAKE_CXX_FLAGS="-mtune=generic -march=x86-64 -O2 -pipe -fstack-protector -D_FORTIFY_SOURCE=2 -g -fPIC" -DCMAKE_LD_FLAGS="-Wl,-O1 -Wl,-z,relro -Wl,--hash-style=gnu -Wl,--as-needed -Wl,--sort-common" -DCMAKE_BUILD_TYPE=RelWithDebInfo -Bbuild -GNinja -DCMAKE_CXX_FLAGS="-Wno-array-bounds -Wno-uninitialized -Wno-unused-variable -Wno-pessimizing-move -Wno-init-list-lifetime -Wno-cast-function-type -Wno-stringop-truncation -Wno-nonportable-include-path" -DCMAKE_SKIP_RPATH=ON -DLLVM_BUILD_RUNTIME=OFF -DLLVM_TOOL_LTO_BUILD=ON -DLLVM_INCLUDE_TESTS=OFF -DLLVM_ENABLE_LIBXML2=OFF -DCMAKE_BUILD_TYPE=Release -DLLVM_INCLUDE_EXAMPLES=OFF -DCOMPILER_RT_BUILD_XRAY=OFF -DCOMPILER_RT_ENABLE_IOS=OFF -DCOMPILER_RT_INCLUDE_TESTS=OFF -DLLVM_INSTALL_TOOLCHAIN_ONLY=ON -DLLVM_ENABLE_PROJECTS='lld;clang' -DLLVM_TARGETS_TO_BUILD='X86;WebAssembly' -DCMAKE_INSTALL_PREFIX=/opt/emscripten-llvm -DCLANG_INCLUDE_TESTS=OFF .." is 0
```

### Runpath analysis düzeltmesi
Eğer CMAKE ile derleme yapılıyorsa confıgure aşamasında aşağıdakini ekleyin:
```python
-DCMAKE_SKIP_BUILD_RPATH=ON
```

### Python paketleri hazırlarken dikkat edilmesi gerekenler.
Eğer python paketi setuptools_scm kullanıyorsa global alana şu komutun eklenmesi gerekmektedir:
```python
shelltools.export("SETUPTOOLS_SCM_PRETEND_VERSION","1.5.0")
```
Python paketleri pypi üzerinden indirilecekse aşağıdaki bağlantı düzenlenerek arşiv çakilmelidir.
```xml
https://pypi.org/packages/source/a/alembic/alembic-1.4.2.tar.gz
```
### Configure bulamıyor AutoReConf aşamasında kalınıyorsa
Büyük olasılıkla aşağıdaki iki paketi kaynak istiyor olabilir:
```xml
<Dependency>autoconf-archive</Dependency>
<Dependency>gettext-devel</Dependency>
```

### Mesontools öntanımlı değerlerine dikkat
Mesontools configure yordamı ile beraber öntanımlı parametreler sunuyor ve rawConfigure gibi bir yordamı yok. O yüzden configure aşamasında aynı parametre yerine geçen bir başka paramtre girersek patlıyoruz. Örneğin `-Dprefix=/usr` kullanırsak hata alıyoruz. Çünkü mesontools arkaplanda varsayılan olarak --prefix kullanıyor. Alınan hata:
```bash
ERROR: Got argument libexecdir as both -Dlibexecdir and --libexecdir. Pick one.
```

### COMAR Betikleri
Eğer COMAR betiği var sa pspec.xml içerisine şu etiketleri atmak gerekiyor:
```xml
        <Provides>
            <COMAR script="service.py">System.Service</COMAR>
            <COMAR script="package.py">System.Package</COMAR>
        </Provides>
```

### Elle include yolu eklemek
Bazen geliştirme başlıkları doğrudan `/usr/include` altında olmaz. Bu durumda cflags veya cxxflags ile eklemek gerekir:
```python
pisitools.cflags.add("-I/usr/include/guile/2.2/")
shelltools.export("CFLAGS", "-I/usr/include/samba-4.0")
```

Örneğin gEDA paketi için şunu yaptım:
```python
    # xorn uses python2 instead of python, shebang problems
    shelltools.export("PYTHON_CFLAGS", "-I/usr/include/python2.7/")
    shelltools.export("PYTHON_LIBS", "-lpython2.7")
    # to fix cannot find libguile.h error
    shelltools.export("GUILE_CFLAGS", "-I/usr/include/guile/2.2/")
    shelltools.export("GUILE_LIBS", "-lguile-2.2")
```

### Temizlik
Her paket yapılırken o paketin kaynağı `/var/cache/pisi/archives/` altına atılır. Her `pisi bi` komutuyla pspsec.xml önce arşiv dosyasını burada arar. Böylece tekrar tekrar aynı dosyayı indirmez. Ama sürekli paket yapılınca burası şişer ve temizlenmesi gerekir:
```bash
rm /var/cache/pisi/archives/*.*
rm /var/cache/pisi/archives/*.gz
rm /var/cache/pisi/archives/*.xz
rm /var/cache/pisi/archives/*.tbz
rm /var/cache/pisi/archives/*.bz2
rm /var/cache/pisi/archives/*.tgz
```

Ayrıca `build` işlemi sonucu gösterdiğinniz yola pisi paketleri atılır. Bunlarla da işiniz yoksa silmeyi unutmayın.

### Diğer Notlar
`make install` bizde aşağıdaki gibi sağlanıyor. Ama bazen elle müdahale gerekebilir:
```python
autotools.rawInstall("DESTDIR=%s" % get.installDIR())
autotools.rawInstall("DESTDIR=%s ETCDIR=%s/etc/3proxy" % (get.installDIR(), get.installDIR()))
```
perl `make install` aiamasında aşağıdaki komutu çalıştırıyor:
DEBUG: return value for "perl Makefile.PL PREFIX=/usr INSTALLDIRS=vendor DESTDIR=/var/pisi/perl-libwww-6.43-5/install" is 0

ActionsAPI'de npm/nodejs apisi yok. Aşağıdaki paket incelenebilir:
```
acorn
```
CMakeTools için aşağıdakiler incelenebilir
```
python3-shiboken
emscripen
keystone
```

## Paketleme dışında konular:

### Yeni yazıyüzü eklemek
oh-my-zsh kullanırken `agnoster` temasında meydana gelen kare karakter sorununu çözmek için:
```
https://github.com/powerline/fonts
```
Komutları izleyin

## Docker Komutları

### Docker ile imaj çekmek
```bash
$ sudo docker pull safaariman/pisi-chroot:latest
latest: Pulling from safaariman/pisi-chroot
b4f23d65a597: Pull complete 
64b246f67221: Pull complete 
5a4d435e21e2: Pull complete 
Digest: sha256:bd8d5004a90fde97667f6adcc2fa459021fff9df5f9cd74c13121411b517f7e8
Status: Downloaded newer image for safaariman/pisi-chroot:latest


# Yeni komut:
$ sudo docker pull pisilinux/chroot
```

### Yüklü imajları listelemek
```bash
$ sudo docker images
REPOSITORY               TAG                 IMAGE ID            CREATED             SIZE
safaariman/pisi-chroot   latest              d45d65605e4e        3 months ago        719MB
safaariman/pisi-chroot   stable              100f9723a6ad        5 months ago        604MB

// alternatif:
$ docker image ls
REPOSITORY               TAG                 IMAGE ID            CREATED             SIZE
safaariman/pisi-chroot   latest              d45d65605e4e        3 months ago        719MB
safaariman/pisi-chroot   stable              100f9723a6ad        5 months ago        604MB

// Sadece imaj IDlerini bastırmak için:
$ sudo docker images -q
$ sudo docker images --quiet
d45d65605e4e
100f9723a6ad
```

### Yüklü imajların hepsini silmek
```bash
$ sudo docker system prune -a
WARNING! This will remove:
        - all stopped containers
        - all networks not used by at least one container
        - all images without at least one container associated to them
        - all build cache
Are you sure you want to continue? [y/N] y
Deleted Images:
untagged: safaariman/pisi-chroot:stable
untagged: safaariman/pisi-chroot@sha256:33f4369879dd0eb36e3c9c053eaab11478e745a74b9bb6648df52e94e6ba01fb
deleted: sha256:100f9723a6ad7bb32ec2586e1dcde8f694de8295009cf45f8e3b04f2017442cc
deleted: sha256:6867dd4ee8b12bc8e8eac3e1e1924b273c55dc2396037b2239f1fda72a84dce0
deleted: sha256:e71080ee36effcb3184421417367edd2cadb01883018d5285a036d942be3ae33
deleted: sha256:53386f70096193f73ea766daac3d7962d30d717eeb1baa27de849d4b4d93c438
untagged: safaariman/pisi-chroot:latest
untagged: safaariman/pisi-chroot@sha256:bd8d5004a90fde97667f6adcc2fa459021fff9df5f9cd74c13121411b517f7e8
deleted: sha256:d45d65605e4e740f98403d79bc0c3ac0ae70bce2b1c5192312b4db42db33ffe3
deleted: sha256:c307352f1ab42e9d38c24577edeb0a34e784e2e96190bfee4b3efcd672d214d9
deleted: sha256:e046052cf1de536539a443f256dd579cba2ceb5ae8c75c59ce096a9913693e89
deleted: sha256:6a4743f21f52d83371e1dcdcb69d006c2483e9e65b3f1acaa765cee585135790

Total reclaimed space: 1.323GB
```

### Docker imajını çalıştırmak
Aşağıdaki bağlantıda çalışma dizinlerini harici harddiske veriyorum:
```bash
$ sudo docker run -v /home/bluedevil/Code/Github/blue-devil/pisi-2.0:/git -v /media/bluedevil/GONULLU/build:/root -v /media/bluedevil/GONULLU/archives:/var/cache/pisi/archives -v /media/bluedevil/GONULLU/packages:/var/cache/pisi/packages -itd --security-opt=seccomp:unconfined safaariman/pisi-chroot:latest bash
```
Yeni komut
```
sudo docker run -v /home/blue/Code/Github/blue-devil/pisi2:/git -v /media/blue/GONULLU/build:/root -v /media/blue/GONULLU/archives:/var/cache/pisi/archives -v /media/blue/GONULLU/packages:/var/cache/pisi/packages -itd --security-opt=seccomp:unconfined pisilinux/chroot:latest bash
```

### Çalışan doker konteynerlerini listelemek:
```bash
$ sudo docker ps
CONTAINER ID        IMAGE                           COMMAND             CREATED             STATUS              PORTS               NAMES
15a0159d26ca        safaariman/pisi-chroot:latest   "bash"              12 seconds ago      Up 7 seconds                            determined_mirzakhani
```
Eğer sonuna parametre olarak `-a` eklersek bu sefer çalışan çalışmayan tüm konteynerler listelenir.

## Çalıştırma

Bu notları çalıştırmak için Pisi GNU/Linux dağıtımına gerek vardır.

## Yazarlar

* **Blue DeviL** - *Reverser* - [bluedevil](http://gitlab.com/bluedevil)

**AnaSayfa** - [SCTZine](http://www.sctzine.com)

## Lisans

Bu proje MIT Lisansı ile lisanlanmıştır.
